#! /bin/sh

# stop on error
set -e

mkdir -p $(pwd)/backup/

mkdir -p ~/Library/Developer/Xcode/UserData/CodeSnippets/
xcode_snippet_dir=$(cd ~/Library/Developer/Xcode/UserData/CodeSnippets/; pwd)

if [[ -L ${xcode_snippet_dir} ]]; then
  rm ${xcode_snippet_dir}
else
  NOW=$(date +"%m_%d_%Y-%M_%S")
  backup_folder="CodeSnippets_${NOW}"

  mv ${xcode_snippet_dir} $(pwd)/backup/${backup_folder}
  echo "Old snippets are backed up to ${backup_folder}"
fi

ln -s $(pwd)/CodeSnippets/ ${xcode_snippet_dir}
